##############################################
# Sample client-side OpenVPN 2.0 config file #
# for connecting to multi-client server.     #
#                                            #
# This configuration can be used by multiple #
# clients, however each client should have   #
# its own cert and key files.                #
#                                            #
# On Windows, you might want to rename this  #
# file so it has a .ovpn extension           #
##############################################

# Specify that we are a client and that we
# will be pulling certain config file directives
# from the server.
client

# Use the same setting as you are using on
# the server.
# On most systems, the VPN will not function
# unless you partially or fully disable
# the firewall for the TUN/TAP interface.
;dev tap
dev tun

# Windows needs the TAP-Win32 adapter name
# from the Network Connections panel
# if you have more than one.  On XP SP2,
# you may need to disable the firewall
# for the TAP adapter.
;dev-node MyTap

# Are we connecting to a TCP or
# UDP server?  Use the same setting as
# on the server.
;proto tcp
proto udp

# The hostname/IP and port of the server.
# You can have multiple remote entries
# to load balance between the servers.
remote 52.254.73.224 1194
;remote my-server-2 1194

# Choose a random host from the remote
# list for load-balancing.  Otherwise
# try hosts in the order specified.
;remote-random

# Keep trying indefinitely to resolve the
# host name of the OpenVPN server.  Very useful
# on machines which are not permanently connected
# to the internet such as laptops.
resolv-retry infinite

# Most clients don't need to bind to
# a specific local port number.
nobind

# Downgrade privileges after initialization (non-Windows only)
;user nobody
;group nogroup

# Try to preserve some state across restarts.
persist-key
persist-tun

# If you are connecting through an
# HTTP proxy to reach the actual OpenVPN
# server, put the proxy server/IP and
# port number here.  See the man page
# if your proxy server requires
# authentication.
;http-proxy-retry # retry on connection failures
;http-proxy [proxy server] [proxy port #]

# Wireless networks often produce a lot
# of duplicate packets.  Set this flag
# to silence duplicate packet warnings.
;mute-replay-warnings

# SSL/TLS parms.
# See the server config file for more
# description.  It's best to use
# a separate .crt/.key file pair
# for each client.  A single ca
# file can be used for all clients.
#ca ca.crt
#cert client.crt
#key client.key

# Verify server certificate by checking that the
# certicate has the correct key usage set.
# This is an important precaution to protect against
# a potential attack discussed here:
#  http://openvpn.net/howto.html#mitm
#
# To use this feature, you will need to generate
# your server certificates with the keyUsage set to
#   digitalSignature, keyEncipherment
# and the extendedKeyUsage to
#   serverAuth
# EasyRSA can do this for you.
remote-cert-tls server

# If a tls-auth key is used on the server
# then every client must also have the key.
tls-auth ta.key 1

# Select a cryptographic cipher.
# If the cipher option is used on the server
# then you must also specify it here.
# Note that 2.4 client/server will automatically
# negotiate AES-256-GCM in TLS mode.
# See also the ncp-cipher option in the manpage
cipher AES-256-CBC

# Enable compression on the VPN link.
# Don't enable this unless it is also
# enabled in the server config file.
#comp-lzo

# Set log file verbosity.
verb 3

# Silence repeating messages
;mute 20
<ca>
-----BEGIN CERTIFICATE-----
MIIE2TCCA8GgAwIBAgIJAMLFGutDbFpaMA0GCSqGSIb3DQEBCwUAMIGjMQswCQYD
VQQGEwJDUjERMA8GA1UECBMIQWxhanVlbGExEDAOBgNVBAcTB1F1ZXNhZGExEjAQ
BgNVBAoTCUNvZGVDaGFpbjESMBAGA1UECxMJQ29kZUNoYWluMRUwEwYDVQQDEwxD
b2RlQ2hhaW4gQ0ExDzANBgNVBCkTBnNlcnZlcjEfMB0GCSqGSIb3DQEJARYQY2FA
Y29kZWNoYWluLmNvbTAeFw0yMDA0MTkwNDI4NTRaFw0zMDA0MTcwNDI4NTRaMIGj
MQswCQYDVQQGEwJDUjERMA8GA1UECBMIQWxhanVlbGExEDAOBgNVBAcTB1F1ZXNh
ZGExEjAQBgNVBAoTCUNvZGVDaGFpbjESMBAGA1UECxMJQ29kZUNoYWluMRUwEwYD
VQQDEwxDb2RlQ2hhaW4gQ0ExDzANBgNVBCkTBnNlcnZlcjEfMB0GCSqGSIb3DQEJ
ARYQY2FAY29kZWNoYWluLmNvbTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoC
ggEBALJjOIdPfAKdmDwgewACuhDOJcscu5KTysPKeKDhBViA71mT7e+PLH3+/hwD
V8PqrV84Oap7xL+AQH2A3yReUJnheKtL5zV5i9ILgLsAyX8lNcHnpo4PzqIbU4Ro
a2+akjqjI6/hXS4J9iStwcmJCX77RYkpTA4XvctV2pDtlas66pufnE4VWpld7S8V
LNrxqZN/Kbefs38cjOMsXnAO6P8zK+XT6osiIkmYPCXbpSvnJdJsYI8HEj0jce+d
AKzdR0X0KLolDRugBf5EB6DNeE7mDWZy9p80isgQm+NQXdYxVsIThQfWpOkuKxhu
D1G1HnT2c03JR6LHZ7jNqy2MzxMCAwEAAaOCAQwwggEIMB0GA1UdDgQWBBQOk9PF
SaUL4LOPWL7lNovu3fBnSjCB2AYDVR0jBIHQMIHNgBQOk9PFSaUL4LOPWL7lNovu
3fBnSqGBqaSBpjCBozELMAkGA1UEBhMCQ1IxETAPBgNVBAgTCEFsYWp1ZWxhMRAw
DgYDVQQHEwdRdWVzYWRhMRIwEAYDVQQKEwlDb2RlQ2hhaW4xEjAQBgNVBAsTCUNv
ZGVDaGFpbjEVMBMGA1UEAxMMQ29kZUNoYWluIENBMQ8wDQYDVQQpEwZzZXJ2ZXIx
HzAdBgkqhkiG9w0BCQEWEGNhQGNvZGVjaGFpbi5jb22CCQDCxRrrQ2xaWjAMBgNV
HRMEBTADAQH/MA0GCSqGSIb3DQEBCwUAA4IBAQB0SCKHSSasL2IgW/+3ca9O3Q7B
We4gcZv2R8BklcBz9P1ARNUkVJmPgjdamyw84QUYlth8U/KIzmBMGeofRlH5sy5X
hBgM4/3VGx6Vh/0Iu0xjOiJGKPvBFFiKq8XZ2x8nYVZKZlKXZXEsWv7It53B2eXm
AvillQHwRQ0RHAi0J4u1Xb/c0vZuuvhIfLGjXEb3TlQSIy48Fcyt+5OBkrFMkG8O
SDfRKLTjZNfQnAkacvYtQVvkmAzZgPoLRtADZkm8rUkPsUI/5oCqFH7jf9W6DdsP
QV4B//2sRnsbLeUtuzjrxeEvswJv5PGInSbs+E3BytCvqxytKlqI4uBZseyJ
-----END CERTIFICATE-----
</ca>
<cert>
Certificate:
    Data:
        Version: 3 (0x2)
        Serial Number: 2 (0x2)
    Signature Algorithm: sha256WithRSAEncryption
        Issuer: C=CR, ST=Alajuela, L=Quesada, O=CodeChain, OU=CodeChain, CN=CodeChain CA/name=server/emailAddress=ca@codechain.com
        Validity
            Not Before: Apr 19 04:58:11 2020 GMT
            Not After : Apr 17 04:58:11 2030 GMT
        Subject: C=CR, ST=Alajuela, L=Quesada, O=CodeChain, OU=CodeChain, CN=client/name=server/emailAddress=ca@codechain.com
        Subject Public Key Info:
            Public Key Algorithm: rsaEncryption
                Public-Key: (2048 bit)
                Modulus:
                    00:bb:8e:09:34:a8:cd:20:68:89:fe:02:66:3b:93:
                    4e:18:d6:15:f4:ea:64:e4:8f:ba:c6:0b:89:b4:c6:
                    06:34:6a:42:26:ab:8f:43:fd:5c:cd:5b:7c:f0:c6:
                    47:c8:2d:71:f5:80:9f:98:24:97:e6:c7:35:45:c8:
                    8e:06:52:d9:08:81:d4:33:9f:37:b5:72:8b:fd:86:
                    f3:38:e8:3e:15:7c:f2:76:81:09:31:d4:58:a2:ea:
                    15:28:38:0e:d0:f3:73:41:49:07:57:30:79:5f:04:
                    52:af:c7:b4:11:29:84:aa:32:e0:d0:11:f6:db:dd:
                    6f:e4:05:2c:c8:96:51:f8:12:49:6d:e8:b5:17:12:
                    df:58:36:fa:99:6f:07:e1:77:2d:86:df:12:37:b7:
                    c0:0a:3d:82:b5:62:82:fd:6f:98:17:67:30:4c:a0:
                    23:8f:13:3a:d6:1b:1a:3e:19:e5:a4:c1:12:b4:8a:
                    d5:f6:bc:3a:ac:33:6f:3c:3b:62:88:10:24:e2:f8:
                    8b:9f:94:3a:95:f1:02:ac:93:96:49:83:39:48:58:
                    87:91:21:b4:70:ed:43:15:a2:47:f9:d5:8d:ee:1a:
                    37:6d:16:12:3f:3a:b5:f1:ee:b5:33:43:47:a1:b1:
                    34:0b:bf:c3:20:4e:37:9b:37:8d:c7:a7:37:a3:44:
                    d9:09
                Exponent: 65537 (0x10001)
        X509v3 extensions:
            X509v3 Basic Constraints: 
                CA:FALSE
            Netscape Comment: 
                Easy-RSA Generated Certificate
            X509v3 Subject Key Identifier: 
                59:67:FE:16:56:02:DB:89:90:86:90:CF:45:74:B6:FA:5D:4C:18:65
            X509v3 Authority Key Identifier: 
                keyid:0E:93:D3:C5:49:A5:0B:E0:B3:8F:58:BE:E5:36:8B:EE:DD:F0:67:4A
                DirName:/C=CR/ST=Alajuela/L=Quesada/O=CodeChain/OU=CodeChain/CN=CodeChain CA/name=server/emailAddress=ca@codechain.com
                serial:C2:C5:1A:EB:43:6C:5A:5A

            X509v3 Extended Key Usage: 
                TLS Web Client Authentication
            X509v3 Key Usage: 
                Digital Signature
            X509v3 Subject Alternative Name: 
                DNS:client
    Signature Algorithm: sha256WithRSAEncryption
         56:86:86:0e:b0:66:6a:07:64:fe:02:a6:21:ff:7d:69:bd:a5:
         b8:55:c3:f8:6a:50:1b:2c:68:3a:55:2c:9d:11:f5:86:64:d4:
         be:55:3f:5c:24:33:25:50:d1:31:7d:77:b8:d0:a7:25:0e:04:
         b2:71:c5:e0:5f:9d:6c:01:f4:d1:66:77:55:3f:78:0c:26:dd:
         43:56:16:88:7c:7e:35:62:ab:6f:7b:64:80:8f:4e:03:52:b6:
         62:b7:d3:50:32:38:60:38:af:af:13:01:38:5d:99:51:4a:6e:
         81:21:9a:2b:06:70:54:8f:de:8f:af:39:01:ad:d6:19:e6:cb:
         2b:86:52:40:20:8a:82:22:0f:27:56:33:f8:6b:b1:5e:1b:2b:
         da:39:9a:33:3d:b4:54:8f:14:6b:ee:57:17:a7:76:68:63:b7:
         49:7a:97:84:a9:41:6d:16:fa:a0:b7:89:01:e7:4b:31:40:6f:
         f3:2c:10:29:bb:be:d1:0c:6b:23:38:ab:30:b1:fd:60:3c:e0:
         a8:9b:58:8a:85:1d:da:92:e2:e9:53:fd:67:27:51:e9:da:a9:
         e8:25:23:d0:62:7a:7d:50:d8:99:07:fc:be:55:c0:63:c4:42:
         df:5f:c7:02:87:0a:df:03:1d:f2:01:5f:36:75:c2:5e:0b:1a:
         cb:f2:5d:7c
-----BEGIN CERTIFICATE-----
MIIFLDCCBBSgAwIBAgIBAjANBgkqhkiG9w0BAQsFADCBozELMAkGA1UEBhMCQ1Ix
ETAPBgNVBAgTCEFsYWp1ZWxhMRAwDgYDVQQHEwdRdWVzYWRhMRIwEAYDVQQKEwlD
b2RlQ2hhaW4xEjAQBgNVBAsTCUNvZGVDaGFpbjEVMBMGA1UEAxMMQ29kZUNoYWlu
IENBMQ8wDQYDVQQpEwZzZXJ2ZXIxHzAdBgkqhkiG9w0BCQEWEGNhQGNvZGVjaGFp
bi5jb20wHhcNMjAwNDE5MDQ1ODExWhcNMzAwNDE3MDQ1ODExWjCBnTELMAkGA1UE
BhMCQ1IxETAPBgNVBAgTCEFsYWp1ZWxhMRAwDgYDVQQHEwdRdWVzYWRhMRIwEAYD
VQQKEwlDb2RlQ2hhaW4xEjAQBgNVBAsTCUNvZGVDaGFpbjEPMA0GA1UEAxMGY2xp
ZW50MQ8wDQYDVQQpEwZzZXJ2ZXIxHzAdBgkqhkiG9w0BCQEWEGNhQGNvZGVjaGFp
bi5jb20wggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQC7jgk0qM0gaIn+
AmY7k04Y1hX06mTkj7rGC4m0xgY0akImq49D/VzNW3zwxkfILXH1gJ+YJJfmxzVF
yI4GUtkIgdQznze1cov9hvM46D4VfPJ2gQkx1Fii6hUoOA7Q83NBSQdXMHlfBFKv
x7QRKYSqMuDQEfbb3W/kBSzIllH4Eklt6LUXEt9YNvqZbwfhdy2G3xI3t8AKPYK1
YoL9b5gXZzBMoCOPEzrWGxo+GeWkwRK0itX2vDqsM288O2KIECTi+IuflDqV8QKs
k5ZJgzlIWIeRIbRw7UMVokf51Y3uGjdtFhI/OrXx7rUzQ0ehsTQLv8MgTjebN43H
pzejRNkJAgMBAAGjggFtMIIBaTAJBgNVHRMEAjAAMC0GCWCGSAGG+EIBDQQgFh5F
YXN5LVJTQSBHZW5lcmF0ZWQgQ2VydGlmaWNhdGUwHQYDVR0OBBYEFFln/hZWAtuJ
kIaQz0V0tvpdTBhlMIHYBgNVHSMEgdAwgc2AFA6T08VJpQvgs49YvuU2i+7d8GdK
oYGppIGmMIGjMQswCQYDVQQGEwJDUjERMA8GA1UECBMIQWxhanVlbGExEDAOBgNV
BAcTB1F1ZXNhZGExEjAQBgNVBAoTCUNvZGVDaGFpbjESMBAGA1UECxMJQ29kZUNo
YWluMRUwEwYDVQQDEwxDb2RlQ2hhaW4gQ0ExDzANBgNVBCkTBnNlcnZlcjEfMB0G
CSqGSIb3DQEJARYQY2FAY29kZWNoYWluLmNvbYIJAMLFGutDbFpaMBMGA1UdJQQM
MAoGCCsGAQUFBwMCMAsGA1UdDwQEAwIHgDARBgNVHREECjAIggZjbGllbnQwDQYJ
KoZIhvcNAQELBQADggEBAFaGhg6wZmoHZP4CpiH/fWm9pbhVw/hqUBssaDpVLJ0R
9YZk1L5VP1wkMyVQ0TF9d7jQpyUOBLJxxeBfnWwB9NFmd1U/eAwm3UNWFoh8fjVi
q297ZICPTgNStmK301AyOGA4r68TAThdmVFKboEhmisGcFSP3o+vOQGt1hnmyyuG
UkAgioIiDydWM/hrsV4bK9o5mjM9tFSPFGvuVxendmhjt0l6l4SpQW0W+qC3iQHn
SzFAb/MsECm7vtEMayM4qzCx/WA84KibWIqFHdqS4ulT/WcnUenaqeglI9Bien1Q
2JkH/L5VwGPEQt9fxwKHCt8DHfIBXzZ1wl4LGsvyXXw=
-----END CERTIFICATE-----
</cert>
<key>
-----BEGIN PRIVATE KEY-----
MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQC7jgk0qM0gaIn+
AmY7k04Y1hX06mTkj7rGC4m0xgY0akImq49D/VzNW3zwxkfILXH1gJ+YJJfmxzVF
yI4GUtkIgdQznze1cov9hvM46D4VfPJ2gQkx1Fii6hUoOA7Q83NBSQdXMHlfBFKv
x7QRKYSqMuDQEfbb3W/kBSzIllH4Eklt6LUXEt9YNvqZbwfhdy2G3xI3t8AKPYK1
YoL9b5gXZzBMoCOPEzrWGxo+GeWkwRK0itX2vDqsM288O2KIECTi+IuflDqV8QKs
k5ZJgzlIWIeRIbRw7UMVokf51Y3uGjdtFhI/OrXx7rUzQ0ehsTQLv8MgTjebN43H
pzejRNkJAgMBAAECggEAZXxX0ty8Ehs+/+lAdGAi1CGTw/DJIzv6roszulOkq9wL
a21NmlaQTraAhrBjyxd8IIG22daDM2b7loUNBLLzqTzHHB+9Pzt6MHdDSLmyTmJL
uH/d/86NH6pWRGsA6WOyK9NeW3lpU1a9f0wqTVcP6CCbsTrnPtU1ZEbAktF0DSy6
0rGRh1w4qslZQzZhsqPd6vcbPJKiAVvWZhWbfISaFLXiap6OuMFPFmaaHQQ5Euue
pBHP/dJJqeEN1z7bhXl0BbLW1g77pkPORpoYNNJhBxmPq1RRnwGxdS58bSV7GY5P
6zVnuYnUwQId+BTvhQaDg45DUGI3Inmex0keOHPnAQKBgQDeFDCyFttsio0q9xl+
fZLHPd1llaF9gVPZy0ec0HbIpNDGNEbuk+RmL5+ICaHg2u3IoR9n70fEoFbJeofo
Adt2vu1NGV1DMBsZPoQ+LXdxp/SSl8UaEHla/K+MdV59fANFZLl2hEC4ZF2rEQ/T
ML5IwxXLhZk9EJf7ubj/h58F8QKBgQDYM9++U7pMvNBnSWYPxYoZr4X3235Lvw5r
sq12Vo19Cl/YWroxEs2in/HM03FvMCglBAo351t1Tw0WxmrSNCl+kuDOn5VkmQWu
6Oj9tOlGKjJiwQ28YhMjzoGdGWh3NTHGZ45po1+lbo/ZPsC4EawgLyOhPvxdE2dE
i3SGYSMMmQKBgQCmOlikxQUjPqYiFeloS9g0+LKimhjR158K8+S7lTKERxHaekDp
zZt1fYodCpC3vUelGC1o36tJm3a6wtO9rr42KP1461ZqtzRhdfEIlYHFeiFQJPgs
5U3g9Cj2qte6d7v/EkDcXGwRWVbfeBrvSpnz3sqOl4a3NZd9nWvDISHAEQKBgHH/
iC5W/5iOxmRvsz0Fr7ezcTQrrABMkVIAOK55HMmdusPdsudFPuBM7bZOlzVFcyVx
Z+HqP7MvJ3umy5RipK50y8ynIKKWfQSpEpET3xx8C20+1ftKy0TKElW/m/GfPsua
tuDEoB4emgK2r+9egh/GO0c+atq47G6cCakBdvWhAoGBAIsLGbHG6GSfGT6woRZT
vhvPlvKLkUXIEeyZ/fxy0UjStwdFBpUh5MCA0SxsN+uKz3RknXGR3Tt7WRYm4msA
QgoqqwJkp9V7vAwhKJjTkRa5kKbJoHkO9oC9w1EQNH69ubvCi2XIMY9H1YwF7Ho5
Y4IcUGFKu/1L8izrHEDAJBUt
-----END PRIVATE KEY-----
</key>
